import java.util.ArrayList;
import java.util.List;

/**
 * Created by hamon on 4/4/17.
 */
public class Action {
    private java.util.List<Line> del,ins;
    private int row,col;

    public List<Line> getDel() {
        return del;
    }

    public List<Line> getIns() {
        return ins;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Action(HCursor c){
        ins = new ArrayList<>();
        del = new ArrayList<>();
        row = c.getrow();
        col = c.getcolumn();
    }

    public boolean isEmpty() {
        return ins.isEmpty() && del.isEmpty();
    }

    public void setPosition(HCursor c){
        row = c.getrow();
        col = c.getcolumn();
    }

    public void setInsert(java.util.List<Line> ins){
        this.ins = ins;
    }

    public  void setDelete(java.util.List<Line> del){
        this.del = del;
    }
}
