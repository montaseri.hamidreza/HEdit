import java.awt.*;


public enum Colorscheme {
    bg(0, 43, 54),
    fg(147, 161, 161),
    cursor(147, 161, 161),
    select(88, 110, 117);

    private final int r, g, b;

    Colorscheme(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public Color getColor() {
        return new Color(r, g, b);
    }

    ;
}
