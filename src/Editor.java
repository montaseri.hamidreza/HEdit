import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.util.List;


public class Editor extends JFrame {
    private java.util.List<Line> lines,clipboard;
    int posR = 0, posC = 0,     //window position related to the lines
            h, w,                   //window size(rows and columns)
            cellH = 25, cellW = 10,     //a char cell width and height
            startX = 50, startY = 50;    //drawing area start position
    private HCursor cursor, selectCursor;
    private Mode mode;
    private boolean select, wordwrap;
    private Stack<Action> undo,redo;

    public Editor(int w, int h) {
        super();
        cursor = new HCursor(this);
        lines = new ArrayList<>();
        clipboard = new ArrayList<>();
        lines.add(new Line(""));
        clipboard.add(new Line(""));
        undo = new Stack<>();
        redo = new Stack<>();
        this.h = h;
        this.w = w;
        this.mode = Mode.INSERT;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Colorscheme.bg.getColor());
        this.addKeyListener(new HKeyListener(this));
        this.setTitle("HEdit");
        this.setBounds(50, 50, w * cellW + startX, h * cellH + startY);
        this.setVisible(true);
        this.setResizable(false);
        this.requestFocus();
    }

    public int gettHeight(){
        return h;
    }

    public int gettWidth() {
        return w;
    }

    public char getCurrentChar(){
        if(cursor.getcolumn()==this.getLine(cursor.getrow()).size())
            return '\n';
        return ((HChar)this.getLine(cursor.getrow()).get(cursor.getcolumn())).getChar();
    }

    public void insert(char c) {
        if((mode==Mode.REPLACE && cursor.getcolumn()<lines.get(cursor.getrow()).size()) || select)
            this.commitDelete(this.delete());
        List<Line> a = new ArrayList<>();
        a.add(new Line(""));
        if(c=='\n')
            a.add(new Line(""));
        else
            a.set(0,new Line(""+c));
        this.insert(a);
        this.commitInsert(a);
        //System.out.println(lines.get(cursor.getrow()).rows(w));
    }

    public void insert(List<Line> l){
        Line c = lines.remove(cursor.getrow());
        List<Line> a = new ArrayList<>();
        for(Line i: l)
            a.add(i.copy());
        for (int i = 0; i < cursor.getcolumn(); i++) {
            a.get(0).add(i,c.remove(0));
        }
        int rr = cursor.getrow()+a.size()-1, cc = a.get(a.size()-1).size();
        while(c.size()>0)
            a.get(a.size()-1).add(c.remove(0));
        while(a.size()>0)
            lines.add(cursor.getrow(),a.remove(a.size()-1));
        cursor.setPosition(rr,cc);
    }

    public void cut(){
        if(select) {
            clipboard = this.delete();
            this.commitDelete(clipboard);
        }
    }

    public void paste(){
        this.insert(clipboard);
        this.commitInsert(clipboard);
    }

    public void copy(){
        if(select){
            clipboard = this.delete();
            this.insert(clipboard);
        }
    }

    public HCursor gettCursor() {
        return cursor;
    }

    public int lines(){
        return lines.size();
    }

    public Line getLine(int index){
        return lines.get(index);
    }

    public void resetWindowPosition(){
        if(!wordwrap) {
            if (cursor.getrow() < posR)
                posR = cursor.getrow();
            if (cursor.getrow() >= posR + h)
                posR = cursor.getrow() - h + 1;
            if (cursor.getcolumn() < posC)
                posC = cursor.getcolumn();
            if (cursor.getcolumn() >= posC + w)
                posC = cursor.getcolumn() - w + 1;
        }
        else {
            if (cursor.getrow() < posR) {
                posR = cursor.getrow();
                posC = 0;
            }
            else if(cursor.getrow() == posR && cursor.getcolumn()<posC)
                posC = 0;
            List<Integer> s = new ArrayList<>();
            s.addAll(lines.get(posR).rows(this.w));
            int cc = s.size(),rr = posR+1;
            while(s.size()>1 && s.get(1)<=posC) {
                s.remove(0);
                cc--;
                if (cc < 1) {
                    posR++;
                    cc = lines.get(posR).rows(this.w).size();
                }
            }

            while(cursor.getrow()>=rr)
                s.addAll(lines.get(rr++).rows(this.w));
            while(s.size()>h) {
                s.remove(0);
                cc--;
                if (cc < 1) {
                    posR++;
                    cc = lines.get(posR).rows(this.w).size();
                }
            }
            posC = s.get(0);
        }
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        g2d.setFont(new Font("Ubuntu Mono", Font.PLAIN, 20));
        g2d.clearRect(0,0, w * cellW + startX, h * cellH + startY);
        if(wordwrap)
            this.wwPaint(g2d);
        else
            this.normalPaint(g2d);
    }

    private void wwPaint(Graphics2D g) {
        int rr = posR,pr = 0;
        Line c = lines.get(rr);
        List<Integer> s = c.rows(w);
        for (int i = 0; i < s.size(); i++) {
            if(s.get(i)>=posC){
                for (int j = 0; (i+1<s.size()?(j + s.get(i) < s.get(i+1)):(j + s.get(i) < c.size())); j++) {
                    ((HChar)c.get(j + s.get(i))).paint(g, startX + j * cellW, startY + pr * cellH,
                            cellW, cellH, isSelected( rr, j + s.get(i)));
                    if(cursor.getrow()==rr && cursor.getcolumn()==j+s.get(i))
                        cursor.paint(g,startX + j * cellW,startY + pr * cellH,cellH);
                }
                pr++;
            }
        }
        if(cursor.getrow()==rr && cursor.getcolumn()==c.size())
            cursor.paint(g,startX + (c.size()-s.get(s.size()-1)) * cellW,startY + (pr-1) * cellH,cellH);
        while(++rr<lines.size() && pr<this.h){
            c = lines.get(rr);
            s = c.rows(w);
            for (int i = 0; i < s.size(); i++) {
                for (int j = 0; (i+1<s.size()?(j + s.get(i) < s.get(i+1)):(j + s.get(i) < c.size())); j++) {
                    ((HChar)c.get(j + s.get(i))).paint(g, startX + j * cellW, startY + pr * cellH,
                            cellW, cellH, isSelected( rr, j + s.get(i)));
                    if(cursor.getrow()==rr && cursor.getcolumn()==j+s.get(i))
                        cursor.paint(g,startX + j * cellW,startY + pr * cellH,cellH);
                }
                pr++;
            }
            if(cursor.getrow()==rr && cursor.getcolumn()==c.size())
                cursor.paint(g,startX + (c.size()-s.get(s.size()-1)) * cellW,startY + (pr-1) * cellH,cellH);
        }

    }

    public void normalPaint(Graphics2D g) {
        resetWindowPosition();
        for (int i = 0; i < h && i + posR < lines.size(); i++) {
            Line tmp = lines.get(i + posR);
            for (int j = 0; j + posC < tmp.size() && j < w; j++) {
                ((HChar) tmp.get(j + posC)).paint(g, startX + j * cellW, startY + i * cellH,
                        cellW, cellH, isSelected(i+posR,j+posC));
            }
        }
        cursor.paint(g,startX + (cursor.getcolumn()-posC) * cellW,startY +(cursor.getrow()-posR) * cellH,cellH);
    }

    public List<Line> delete() {
        if(!select)
        {
            this.selectOn(cursor.copy());
            cursor.right();
        }
        if(cursor.getrow()>selectCursor.getrow() ||
                (cursor.getrow()==selectCursor.getrow() && cursor.getcolumn()>selectCursor.getcolumn())){
            HCursor tmp = cursor;
            cursor = selectCursor;
            selectCursor = tmp;
        }
        List<Line> res = new ArrayList<>();
        for(int i=cursor.getrow(); i<selectCursor.getrow()+1; i++)
            res.add(lines.remove(cursor.getrow()));
        Line tmp = new Line("");
        int t = res.get(res.size()-1).size()-selectCursor.getcolumn();
        for (int i = 0; i < cursor.getcolumn(); i++) {
            tmp.add(res.get(0).remove(0));
        }
        int s = res.get(res.size()-1).size()-t;
        for (int i = 0; i < t; i++) {
            tmp.add(res.get(res.size()-1).remove(s));
        }
        lines.add(cursor.getrow(),tmp);
        this.selectOff();
        return res;
    }

    public void back() {
        if(select)
            this.commitDelete(this.delete());
        else if(cursor.getcolumn()>0 || cursor.getrow()>0) {
            cursor.left();
            this.commitDelete(this.delete());
        }
    }

    public void changeMode() {
        if(this.mode!=Mode.INSERT)
            this.mode = Mode.INSERT;
        else
            this.mode = Mode.REPLACE;
    }

    public boolean selectMode() {
        return this.select;
    }

    public void selectOn(HCursor selectCursor) {
        this.selectCursor = selectCursor;
        select = true;
    }

    public void selectOff() {
        select = false;
    }

    private boolean isSelected(int r, int c){
        if(!select)
            return false;
        if(((r>selectCursor.getrow() || (r==selectCursor.getrow() && c>=selectCursor.getcolumn())) &&
                (r<cursor.getrow() || (r==cursor.getrow() && c<cursor.getcolumn()))) ||
                ((r>cursor.getrow() || (r==cursor.getrow() && c>=cursor.getcolumn())) &&
                        (r<selectCursor.getrow() || (r==selectCursor.getrow() && c<selectCursor.getcolumn()))))
            return true;
        return false;
    }

    public void deleteLine() {
        cursor.lineBeginning();
        selectOn(cursor.copy());
        cursor.lineEnd();
        cursor.right();
        this.commitDelete(this.delete());
    }

    public void selectAll() {
        cursor.end();
        this.selectOn(cursor.copy());
        cursor.beginning();
    }

    public void newAction() {
        if(undo.size()>100)
            undo.remove(undo.firstElement());
        undo.push(new Action(cursor));

    }

    public void commitInsert(List<Line> a){
        Action tmp = undo.peek();
        tmp.setInsert(a);
        redo.clear();
    }

    public void commitDelete(List<Line> a){
        Action tmp = undo.peek();
        tmp.setDelete(a);
        if(a.size()>0 && a.get(0).size()>0) {
            tmp.setPosition(cursor);
        }
        redo.clear();
    }

    public void undo(){
        if(undo.isEmpty())
            return;
        Action tmp = undo.pop();
        if(tmp.isEmpty()) {
            this.undo();
            return;
        }
        undo.push(new Action(cursor));
        cursor.setPosition(tmp.getRow(),tmp.getCol());
        if(!tmp.getIns().isEmpty()) {
            this.selectOn(cursor.copy());
            List<Line> ins = tmp.getIns();
            int cc = (ins.size() == 1 ? ins.get(0).size() + cursor.getcolumn() : ins.get(ins.size() - 1).size());
            cursor.setPosition(cursor.getrow() + ins.size() - 1, cc);
            this.delete();
        }
        if(!tmp.getDel().isEmpty())
            this.insert(tmp.getDel());
        undo.pop();
        redo.push(tmp);
    }

    public void redo() {
        try{
            Action tmp = redo.pop();
            undo.push(new Action(cursor));
            cursor.setPosition(tmp.getRow(),tmp.getCol());
            if(!tmp.getDel().isEmpty()) {
                this.selectOn(cursor.copy());
                List<Line> ins = tmp.getDel();
                int cc = (ins.size() == 1 ? ins.get(0).size() + cursor.getcolumn() : ins.get(ins.size() - 1).size());
                cursor.setPosition(cursor.getrow() + ins.size() - 1, cc);
                this.delete();
            }
            if(!tmp.getIns().isEmpty())
                this.insert(tmp.getIns());
            undo.pop();
            undo.push(tmp);
        }
        catch (EmptyStackException e){
            return;
        }
    }

    public void switchWordWrap() {
        posC = 0;
        wordwrap = (wordwrap?false:true);
    }

    public boolean wordwrap() {
        return wordwrap;
    }
}
