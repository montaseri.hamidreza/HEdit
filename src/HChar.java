import java.awt.*;

public class HChar {
    char value;

    public HChar(char value) {
        this.value = value;
    }

    public char getChar(){
        return value;
    }

    @Override
    public String toString() {
        return Character.toString(value);
    }

    public void paint(Graphics2D g, int x, int y, int w, int h, boolean select) {

        if(select) {
            g.setColor(Colorscheme.select.getColor());
            g.fillRect(x, (int)(y - h*0.8), w, h);
        }
        g.setColor(Colorscheme.fg.getColor());
        g.drawString(this.toString(), x, y);
    }

    public HChar copy() {
        return new HChar(value);
    }
}
