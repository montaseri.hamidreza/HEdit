import java.awt.*;

public class HCursor {
    private Editor e;
    private int row, col;

    public HCursor(Editor e, int row, int col) {
        this.col = col;
        this.row = row;
        this.e = e;
    }

    public HCursor(Editor e) {
        this(e, 0, 0);
    }

    public void setPosition(int row, int col){
        this.row = row;
        this.col = col;
    }

    public int getrow() {
        return row;
    }

    public int getcolumn() {
        return col;
    }

    public void left(){
        col--;
        if(col<0){
            if(row>0){
                row--;
                col = e.getLine(row).size();
            }
            else
                col=0;
        }
    }

    public void right(){
        col++;
        if(col>e.getLine(row).size()) {
            if (row + 1 < e.lines()) {
                row++;
                col = 0;
            } else
                col = e.getLine(row).size();
        }
    }

    public void up(){
        if(!e.wordwrap()){
            if(row>0)
                row--;
            if(col>e.getLine(row).size())
                col = e.getLine(row).size();
        }
        else {
            Line c = e.getLine(row);
            java.util.List<Integer> s = c.rows(e.gettWidth());
            for (int i = s.size()-1; i > 0 ; i--) {
                if(col>=s.get(i)){
                    col = (col-s.get(i)) + s.get(i-1);
                    if(col>=s.get(i))
                        col = s.get(i)-1;
                    return;
                }
            }
            if(row > 0) {
                row--;
                c = e.getLine(row);
                s = c.rows(e.gettWidth());
                col = col + s.get(s.size()-1);
                if(col>c.size())
                    col = c.size();
            }
        }
    }

    public void down() {
        if(e.wordwrap()){
            Line c = e.getLine(row);
            java.util.List<Integer> s = c.rows(e.gettWidth());
            for (int i = s.size()-2; i >= 0 ; i--) {
                if(col>=s.get(i) && col<s.get(i+1)){
                    col = (col-s.get(i)) + s.get(i+1);
                    if((i+2>=s.size() && col>c.size()))
                        col = c.size();
                    else if(i+2<s.size() && col>=s.get(i+2))
                        col = s.get(i+2)-1;
                    return;
                }
            }
            if(row+1 < e.lines()) {
                row++;
                c = e.getLine(row);
                col = col - s.get(s.size()-1);
                if(col>c.size())
                    col = c.size();
            }
        }
        else {
            if (row + 1 < e.lines())
                row++;
            if (col > e.getLine(row).size())
                col = e.getLine(row).size();
        }
    }

    public void paint(Graphics2D g, int x, int y, int h) {
        Color tmp = g.getColor();
        g.setColor(Colorscheme.cursor.getColor());
        g.fillRect(x,y-h,2,h);
        g.setColor(tmp);
    }

    public void lineBeginning() {
        col = 0;
    }

    public void lineEnd(){
        col = e.getLine(row).size();
    }

    public void beginning() {
        col = 0;
        row = 0;
    }

    public void end(){
        row = e.lines()-1;
        col = e.getLine(row).size();
    }

    public void pageUp() {
        row = Math.max(0,row-e.gettHeight());
        this.lineBeginning();
    }

    public void pageDown() {
        row = Math.min(e.lines()-1,row+e.gettHeight());
        this.lineEnd();
    }

    public void wordLeft() {
        do {
            this.left();
        }while(!Character.isLetterOrDigit(e.getCurrentChar()) && (row>0 || col>0));

        while(Character.isLetterOrDigit(e.getCurrentChar()) && (row>0 || col>0))
            this.left();
        if(!Character.isLetterOrDigit(e.getCurrentChar()))
            this.right();
    }

    public void wordRight() {
        while(!Character.isLetterOrDigit(e.getCurrentChar())
                && !(row==e.lines()-1 && col==e.getLine(e.lines()-1).size()))
            this.right();

        while(Character.isLetterOrDigit(e.getCurrentChar())
                && !(row==e.lines()-1 && col==e.getLine(e.lines()-1).size())){
                System.out.println(col);
                this.right();
        }
    }

    public HCursor copy() {
        return new HCursor(e,row,col);
    }
}
