
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class HKeyListener implements KeyListener {
    Editor editor;

    public HKeyListener(Editor editor) {
        this.editor = editor;
    }

    public void keyTyped(KeyEvent e) {

        editor.repaint();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        editor.newAction();
        HCursor c = editor.gettCursor(), cur = c.copy();
        boolean nav = false,act = false;
        switch( e.getKeyCode()) {
            case KeyEvent.VK_W:
                if(e.isControlDown())
                    editor.switchWordWrap();
                break;
            case KeyEvent.VK_R:
                if(e.isControlDown())
                    editor.redo();
                break;
            case KeyEvent.VK_Z:
                if(e.isControlDown())
                    editor.undo();
                break;
            case KeyEvent.VK_A:
                if(e.isControlDown())
                    editor.selectAll();
                act = true;
                break;
            case KeyEvent.VK_X:
                if(e.isControlDown())
                    editor.cut();
                act = true;
                break;
            case KeyEvent.VK_V:
                if(e.isControlDown())
                    editor.paste();
                act = true;
                break;
            case KeyEvent.VK_C:
                if(e.isControlDown())
                    editor.copy();
                break;
            case KeyEvent.VK_D:
                if(e.isControlDown())
                    editor.deleteLine();
                act = true;
                break;
            case KeyEvent.VK_INSERT:
                editor.changeMode();
                act = true;
                break;
            case KeyEvent.VK_DELETE:
                editor.commitDelete(editor.delete());
                act = true;
                break;
            case KeyEvent.VK_BACK_SPACE:
                act = true;
                editor.back();
                break;
            case KeyEvent.VK_LEFT:
                if(e.isControlDown())
                    c.wordLeft();
                else
                    c.left();
                nav = true;
                break;
            case KeyEvent.VK_RIGHT :
                if(e.isControlDown())
                    c.wordRight();
                else
                    c.right();
                nav = true;
                break;
            case KeyEvent.VK_UP:
                c.up();
                nav = true;
                break;
            case KeyEvent.VK_DOWN:
                c.down();
                nav = true;
                break;
            case KeyEvent.VK_HOME:
                if(e.isControlDown())
                    c.beginning();
                else
                    c.lineBeginning();
                nav = true;
                break;
            case KeyEvent.VK_END:
                if(e.isControlDown())
                    c.end();
                else
                    c.lineEnd();
                nav = true;
                break;
            case KeyEvent.VK_PAGE_UP:
                c.pageUp();
                nav = true;
                break;
            case KeyEvent.VK_PAGE_DOWN:
                c.pageDown();
                nav = true;
                break;
        }

        if (Character.isDefined(e.getKeyChar()) && e.getKeyCode()!=KeyEvent.VK_BACK_SPACE &&
            e.getKeyCode()!=KeyEvent.VK_DELETE && !e.isControlDown()) {
            editor.insert(e.getKeyChar());
            act = true;
        }

        if(nav && e.isShiftDown())
        {
            if(!editor.selectMode())
                editor.selectOn(cur);
        }
        else if(e.getKeyCode()!=KeyEvent.VK_SHIFT && !e.isControlDown())
            editor.selectOff();
        editor.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
