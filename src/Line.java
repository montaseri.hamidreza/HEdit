import java.util.ArrayList;
import java.util.List;


public class Line extends ArrayList {
    public Line(String str) {
        super();
        for (int i = 0; i < str.length(); i++) {
            this.add(new HChar(str.charAt(i)));
        }
    }

    @Override
    public Line subList(int s, int f){
        Line r = new Line("");
        for (int i = s; i <f ; i++) {
            r.add(this.get(i));
        }
        return r;
    }

    public Line copy(){
        Line res = new Line("");
        for(Object c: this)
            res.add(((HChar)c).copy());
        return res;
    }

    public List<Integer> rows(int width) {
        List<Integer> res = new ArrayList<>();
        boolean ch = false;
        res.add(0);
        for (int i = 0, c = 1, b = 0; i < this.size(); i++,c++) {
            if(((HChar)this.get(i)).value==' ')
                ch =true;
            else if(ch) {
                b = i;
                ch = false;
            }
            if(c>=width){
                if(res.get(res.size()-1)==b)
                    b = i;
                res.add(b);
                c = i - b + 1;
            }
        }
        return res;
    }
}
